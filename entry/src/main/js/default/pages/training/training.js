import router from '@system.router';

var picker1value = null;
var picker2value = null;
var picker1seconds = null;
var picker2seconds = null;
var time1 = null;
var time2 = null;
var time3 = null;
var counter = 0;

export default {
    data: {
        seconds: 0,
        isShow: true,
        breath: "吸气",
        percent: 0,
        duration: "",
        count: ""
    },
    clickAction() {
        clearInterval(time1);
        time1 = null;
        clearInterval(time2);
        time2 = null;
        clearInterval(time3);
        time3 = null;
        router.replace({
            uri: "pages/index/index"
        })
    },
    onInit() {
        console.log("训练页面的onInit()正在被调用");
        console.log("接收到的左边的值：" + this.key1);
        console.log("接收到的右边的值：" + this.key2);
        picker1value = this.key1;
        picker2value = this.key2;

        picker1seconds = picker1value * 60;
        if (picker2value == "较慢") {
            picker2seconds = 6;
        } else if (picker2value == "舒缓") {
            picker2seconds = 4;
        } else if (picker2value = "较快") {
            picker2seconds = 2;
        }
        this.seconds = picker1seconds;
        this.duration = picker2seconds + "s";
        this.count = (picker1seconds / picker2seconds).toString();
    },
    run1() {
        this.seconds--;
        if (this.seconds == 0) {
            clearInterval(time1);
            time1 = null;
            this.isShow = false;
        }
    },
    run2() {
        counter++;
        if (counter == picker1seconds / picker2seconds) {
            clearInterval(time2);
            time2 = null;
            this.breath = "已完成";
        } else {
            if (this.breath == "吸气") {
                this.breath = "呼气";
            } else if (this.breath == "呼气") {
                this.breath = "吸气";
            }
        }
    },
    run3() {
        this.percent = (parseInt(this.percent) + 1).toString();
        if (parseInt(this.percent) < 10) {
            this.percent = "0" + this.percent;
        } else if (parseInt(this.percent) == 100) {
            this.percent = "0";
        }
        if (time2 == null) {
            clearInterval(time3);
            time3 = null;
            this.percent = "100";
        }
    },
    onReady() {
        console.log("训练页面的onReady()正在被调用");
        time1 = setInterval(this.run1,1000);
        time2 = setInterval(this.run2,picker2seconds * 1000);
        time3 = setInterval(this.run3,picker2seconds / 100 * 1000);
    },
    onShow() {
        console.log("训练页面的onShow()正在被调用");
    },
    onDestroy() {
        console.log("训练页面的onDestroy()正在被调用");
    },
    toReport1Page(e){
        if(e.direction=="right"){
            router.replace({
                uri:"pages/report1/report1"
            });
        }
    }
}