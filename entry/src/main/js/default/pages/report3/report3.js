import router from '@system.router';

export default {
    data: {
        timeRange: ["07:00", "12:00", "17:00", "22:00"],
        activityData: [{
                           iconName: "red",
                           text: "活动",
                           percent: 0
                       },
                       {
                           iconName: "gray",
                           text: "静止",
                           percent: 0
                       }],
        options: {
            xAixs: {
                axisTick: 20
            },
            yAixs: {
                max: 1,
                axisTick: 1
            }
        },
        datasets: [{
                       data: []
                   }],
        datasetsStatic: [{
                             fillColor: "#696969",
                             data: []
                         }]
    },
    onInit() {
        let activies = [];
        let activiesStatic = [];
        for (let index = 0; index < 20; index++) {
            let rand = this.getRandomZeroOrOne();
            activies.push(rand);
            activiesStatic.push(Math.abs(rand -1));
        }
        this.datasets[0].data = activies;
        this.activiesStatic[0].data = activiesStatic;
        this.countActivityPercent(activies);
    },
    getRandomZeroOrOne() {
        return Math.floor(Math.random() + 0.5)
    },
    countActivityPercent(activies) {
        let count = 0;
        for (let index = 0; index < activies.length; index++) {
            if (activies[index] == 1) {
                count++;
            }
        }
        this.activityData[0].percent = Math.round(count / activies.length * 100);
        this.activityData[1].percent = 100 - this.activityData[0].percent;
    },
    toNextPage(e) {
        switch (e.direction) {
            case 'left':
            router.replace({
                uri: "pages/index/index"
            })
            break;
            case 'up':
            router.replace({
                uri: "pages/report4/report4"
            })
            break;
            case "down":
            router.replace({
                uri: "pages/report2/report2"
            })
        }
    }
}
